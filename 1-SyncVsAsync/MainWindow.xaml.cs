﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _1_SyncVsAsync
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly int _sleepPeriod = 5000;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnSync(object sender, RoutedEventArgs e)
        {
            StatusText.Text = "Processing...";
            Thread.Sleep(_sleepPeriod);
            StatusText.Text = String.Empty;
        }

        private async void OnAsync(object sender, RoutedEventArgs e)
        {
            StatusText.Text = "Processing...";
            await Task.Delay(_sleepPeriod);
            StatusText.Text = String.Empty;
        }
    }
}
