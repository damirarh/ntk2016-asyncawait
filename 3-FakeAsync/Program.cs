﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace _3_FakeAsync
{
    class Program
    {
        private static readonly int _iterations = 50;
        private static readonly AsyncService AsyncService = new AsyncService();

        static void Main(string[] args)
        {
            var tasks = new Task[_iterations];
            var stopwatch = new Stopwatch();

            Console.WriteLine("Running true async methods:");
            Console.ReadLine();

            stopwatch.Start();
            for (int i = 0; i < _iterations; i++)
            {
                tasks[i] = AsyncService.TrueAsync(i);
            }
            Task.WaitAll(tasks);
            stopwatch.Stop();
            var trueAsyncDuration = stopwatch.ElapsedMilliseconds;

            Console.WriteLine("Running fake async methods:");
            Console.ReadLine();

            stopwatch.Restart();
            for (int i = 0; i < _iterations; i++)
            {
                tasks[i] = AsyncService.FakeAsync(i);
            }
            Task.WaitAll(tasks);
            stopwatch.Stop();
            var fakeAsyncDuration = stopwatch.ElapsedMilliseconds;

            Console.WriteLine("Results:");
            Console.ReadLine();

            Console.WriteLine($"True async duration: {trueAsyncDuration} ms");
            Console.WriteLine($"Fake async duration: {fakeAsyncDuration} ms");
            Console.ReadLine();
        }
    }
}
