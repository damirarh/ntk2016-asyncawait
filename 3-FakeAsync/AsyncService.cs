﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace _3_FakeAsync
{
    public class AsyncService
    {
        private readonly int Millis = 250;

        public async Task<string> TrueAsync(int index)
        {
            Console.WriteLine($"Start {index}");
            await Task.Delay(Millis);
            Console.WriteLine($"End {index}");
            return string.Empty;
        }

        public async Task<string> FakeAsync(int index)
        {
            return await Task.Run(() =>
            {
                Console.WriteLine($"Start {index}");
                Thread.Sleep(Millis);
                Console.WriteLine($"End {index}");
                return String.Empty;
            });
        }
    }
}